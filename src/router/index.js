import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import WPI from '../views/wpi.vue'
import edit_mode from '../views/edit.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/wpi',
    name: 'WPI',
    component: WPI
  },
  {
    path: '/edit',
    name: 'edit_mode',
    component: edit_mode
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
