# ccportal-front

## Overview

Here's a general view of the app on the Working Group I page. The WPI page is divided into topics, a topic is compound of questions, and a question is compound of bullet points that may be followed by interactive data.

![](ccportal-overview.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### For Docker

```
docker build . -t ccportal-front
```

```
docker run -d --name ccportal-front -p 8080:80 ccportal-front
```
